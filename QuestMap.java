package game.space.quest.editor;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Circle;

import java.util.*;


public class QuestMap extends ScrollPane {
    private final Pane pane;
    private Script startScript=new Script();
    private Script statsInfoScript=new Script();
    private Script edgeScript=new Script();
    private final Pane contextPane;

    public QuestMap() {
        pane = new Pane();
        pane.setMinWidth(10000);
        pane.setMinHeight(10000);
        contextPane = new Pane();
        contextPane.setOpacity(0);
        contextPane.setMinWidth(1);
        contextPane.setMinHeight(1);
        contextPane.setScaleX(10000);
        contextPane.setScaleY(10000);
        contextPane.setTranslateX(5000);
        contextPane.setTranslateY(5000);
        pane.getChildren().addAll(contextPane);


        setHvalue(0.5);
        setVvalue(0.5);

        ContextMenu contextMenu=new ContextMenu();
        MenuItem createLocation = new MenuItem("New location");
        contextMenu.getItems().addAll(createLocation);

        createLocation.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                MousePosition userData = (MousePosition) contextMenu.getUserData();
                createLocation(userData.x,userData.y);
            }
        });

        contextPane.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.isSecondaryButtonDown()){
                    contextMenu.show(contextPane,mouseEvent.getScreenX(),mouseEvent.getSceneY());
                    contextMenu.setUserData(new MousePosition(mouseEvent.getX()*10000,mouseEvent.getY()*10000));
                }else{
                    contextMenu.hide();
                }
            }
        });

        setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.isMiddleButtonDown()){
                    QuestMap.this.setPannable(true);
                }
            }
        });
        setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                QuestMap.this.setPannable(false);
            }
        });

        pane.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(!mouseEvent.isControlDown() && mouseEvent.isPrimaryButtonDown()) {
                    if (mouseEvent.getTarget() instanceof Location) {
                        ((Location) mouseEvent.getTarget()).updatePos(mouseEvent.getX(), mouseEvent.getY());
                    }
                }
            }
        });

        setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if(keyEvent.getCode()== KeyCode.ADD){
                    pane.setScaleX(pane.getScaleX()*1.2);
                    pane.setScaleY(pane.getScaleY()*1.2);
                    QuestMap questMap = QuestMap.this;
                    questMap.setVvalue((questMap.getVvalue()-0.5)*1.2+0.5);
                    questMap.setHvalue((questMap.getHvalue()-0.5)*1.2+0.5);
                }
                if(keyEvent.getCode()==KeyCode.SUBTRACT){
                    pane.setScaleX(pane.getScaleX()/1.2);
                    pane.setScaleY(pane.getScaleY()/1.2);
                    QuestMap questMap = QuestMap.this;
                    questMap.setVvalue((questMap.getVvalue()-0.5)/1.2+0.5);
                    questMap.setHvalue((questMap.getHvalue()-0.5)/1.2+0.5);
                }
            }
        });


        this.setContent(pane);
    }

    public Location createLocation(double x,double y){
        Location location =new Location(this,x,y);
        pane.getChildren().addAll(location);
        return location;
    }

    public void deleteLocation(Location location){
        trash.add(location);
        for (Node node : pane.getChildren()) {
            if(node instanceof Jump){
                Jump jump = (Jump) node;
                if(jump.getTo()== location || jump.getFrom()== location){
                    trash.add(node);
                    location.removeJump(jump);
                }
            }
        }
        removeTrash();
    }

    public Jump createJump(Location l1, Location l2){
        Jump jump = new Jump(this,l1,l2);
        pane.getChildren().addAll(jump);
        l1.addJumpTo(jump);
        l2.addJumpFrom(jump);
        sortPane();
        return jump;
    }

    public void deleteJump(Jump jump){
        trash.add(jump);
        for (Node node : pane.getChildren()) {
            if(node instanceof Location){
                ((Location) node).removeJump(jump);
            }
        }
        removeTrash();
    }

    private Set<Node> trash =new HashSet<>();
    public void removeTrash(){
        pane.getChildren().removeAll(trash);
    }

    private static final HashMap<Class,Integer> node_level=new HashMap<>();
    static {
        node_level.put(Circle.class,3);
        node_level.put(Location.class,2);
        node_level.put(Arc.class,1);
        node_level.put(Jump.class,0);
        node_level.put(Pane.class,-1);
    }

    public void sortPane(){
        ObservableList<Node> nodes = FXCollections.observableArrayList(pane.getChildren());
        Collections.sort(nodes, new Comparator<Node>() {
            @Override
            public int compare(Node n1, Node n2) {
                return node_level.get(n1.getClass())-node_level.get(n2.getClass());
            }
        });
        pane.getChildren().setAll(nodes);
    }

    public class MousePosition{
        private double x;
        private double y;

        public MousePosition(double x, double y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "MousePosition{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }

    public Location findStartLocation() {
        for (Node node : pane.getChildren()) {
            if(node instanceof Location){
                if(((Location) node).getType()== Location.LocationType.START){
                    return ((Location) node);
                }
            }
        }
        return null;
    }

    public Script getStartScript() {
        return startScript;
    }

    public Pane getPane() {
        return pane;
    }

    public void clear(Circle circle){
        startScript=new Script();
        statsInfoScript=new Script();
        edgeScript=new Script();
        pane.getChildren().clear();
        pane.getChildren().addAll(contextPane);
        pane.getChildren().addAll(circle);
    }

    public Script getStatsInfoScript() {
        return statsInfoScript;
    }

    public void centerScroll(Circle circle){
        Bounds bounds = pane.localToParent(circle.getBoundsInParent());
        setHvalue(bounds.getMaxX()/10000);
        setVvalue(bounds.getMaxY()/10000);
    }

    public Script getEdgeScript() {
        return edgeScript;
    }

    public void setEdgeScript(Script edgeScript) {
        this.edgeScript = edgeScript;
    }
}
