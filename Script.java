package game.space.quest.editor;

/**
 * Created by spix-rayt on 3/6/16.
 */
public class Script {
    private String script="";

    public Script() {
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }
}
