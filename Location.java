package game.space.quest.editor;

import game.space.quest.editor.proto.ProtoQuest;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.input.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.TextAlignment;

import java.util.ArrayList;
import java.util.HashMap;


public class Location extends Circle {
    private static final LocationEdit locationEdit=new LocationEdit();
    private double r=6;
    private QuestMap parent;

    private ArrayList<Jump> jumpShapesTo =new ArrayList<>();
    private ArrayList<Jump> jumpShapesFrom=new ArrayList<>();
    private final Tooltip tooltip;
    private LocationType type=LocationType.NORMAL;

    private static Location clip_copy=null;

    enum LocationType{
        NORMAL,
        START,
        DEFEAT,
        VICTORY
    }

    public Location(QuestMap parent, double centerX, double centerY) {
        super(centerX, centerY, 1);
        setRadius(r);
        this.parent = parent;

        tooltip = new Tooltip();
        tooltip.setTextAlignment(TextAlignment.CENTER);
        tooltip.setWrapText(true);
        tooltip.setMaxWidth(800);

        setOnDragDetected(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.isPrimaryButtonDown()) {
                    if(mouseEvent.isControlDown()){
                        Dragboard db = startDragAndDrop(TransferMode.COPY);
                        ClipboardContent clipboardContent = new ClipboardContent();
                        clipboardContent.putString("");
                        db.setContent(clipboardContent);
                        mouseEvent.consume();
                    }
                }
            }
        });

        setOnDragOver(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent dragEvent) {
                if (dragEvent.getGestureSource() != Location.this) {
                    dragEvent.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                }

                dragEvent.consume();
            }
        });

        setOnDragEntered(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent dragEvent) {
                if (dragEvent.getGestureSource() != Location.this) {
                    Location.this.setRadius(r * 1.4);
                }

                dragEvent.consume();
            }
        });

        setOnDragExited(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent dragEvent) {
                if (dragEvent.getGestureSource() != Location.this) {
                    Location.this.setRadius(r);
                }

                dragEvent.consume();
            }
        });

        setOnDragDropped(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent dragEvent) {
                Dragboard db = dragEvent.getDragboard();
                boolean success = false;
                if (db.hasString()) {
                    parent.createJump(((Location) dragEvent.getGestureSource()), ((Location) dragEvent.getGestureTarget()));
                    success = true;
                }
                dragEvent.setDropCompleted(success);

                dragEvent.consume();
            }
        });

        setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                Location.this.setRadius(r * 1.4);
                tooltip.show(Location.this,mouseEvent.getScreenX()-tooltip.getWidth()/2,mouseEvent.getScreenY()-tooltip.getHeight()*1.1);
            }
        });

        setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                Location.this.setRadius(r);
                tooltip.hide();
            }
        });

        ContextMenu contextMenu = new ContextMenu();
        MenuItem menuItem = new MenuItem("Delete");
        MenuItem menuItem1 = new MenuItem("Edit");
        MenuItem copy_menu = new MenuItem("Copy");
        MenuItem paste_menu = new MenuItem("Paste");
        MenuItem menustartloc = new MenuItem("Set start location");
        MenuItem menudefeatloc = new MenuItem("Set defeat location");
        MenuItem menunormalloc = new MenuItem("Set normal location");
        MenuItem menuvictoryloc = new MenuItem("Set victory location");

        contextMenu.getItems().addAll(menuItem1,copy_menu,paste_menu,menuItem,new SeparatorMenuItem(),menunormalloc,menustartloc,menuvictoryloc,menudefeatloc);

        menuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                delete();
            }
        });

        menuItem1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                edit();
            }
        });
        copy_menu.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                clip_copy=Location.this;
            }
        });
        paste_menu.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Location.this.setType(clip_copy.getType());
                Location.this.setLocationText(clip_copy.locationText);
                Location.this.setLocationCode(clip_copy.getLocationCode());
            }
        });
        menustartloc.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                setType(LocationType.START);
            }
        });

        menudefeatloc.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                setType(LocationType.DEFEAT);
            }
        });
        menunormalloc.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                setType(LocationType.NORMAL);
            }
        });
        menuvictoryloc.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                setType(LocationType.VICTORY);
            }
        });


        setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.isSecondaryButtonDown()){
                    if(getType()==LocationType.START){
                        menustartloc.setDisable(true);
                    }else{
                        menustartloc.setDisable(false);
                    }
                    if(getType()==LocationType.NORMAL){
                        menunormalloc.setDisable(true);
                    }else{
                        menunormalloc.setDisable(false);
                    }
                    if(getType()==LocationType.DEFEAT){
                        menudefeatloc.setDisable(true);
                    }else{
                        menudefeatloc.setDisable(false);
                    }
                    if(getType()==LocationType.VICTORY){
                        menuvictoryloc.setDisable(true);
                    }else{
                        menuvictoryloc.setDisable(false);
                    }
                    if(clip_copy==null){
                        paste_menu.setDisable(true);
                    }else{
                        paste_menu.setDisable(false);
                    }
                    contextMenu.show(Location.this,mouseEvent.getScreenX(),mouseEvent.getScreenY());
                }else {
                    contextMenu.hide();
                }
            }
        });

        setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2) {
                        edit();
                    }
                }
            }
        });

        updateColor();
        updateTooltipText();
    }

    public void edit(){
        locationEdit.load(this);
    }

    private String locationText="",locationCode="";

    public String getLocationText() {
        return locationText;
    }

    public String getLocationCode() {
        return locationCode;
    }


    public void setLocationText(String locationText) {
        this.locationText = locationText;
        updateTooltipText();
        updateColor();
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
        updateTooltipText();
    }

    private void updateTooltipText(){
        String loct,locc;
        if(locationText.length()>400){
            loct=locationText.substring(0,400)+"...";
        }else{
            loct=locationText;
        }
        if(locationCode.length()>400){
            locc=locationCode.substring(0,400)+"...";
        }else{
            locc=locationCode;
        }
        tooltip.setText(loct+"\n·\n"+locc);
    }





    public void delete(){
        parent.deleteLocation(this);
    }

    public void addJumpTo(Jump jump){
        jumpShapesTo.add(jump);
        updateJumpDeviations();
    }

    public void updateJumpDeviations(){
        HashMap<Location,Double> deviationHashMap=new HashMap<>();
        for (Jump shape : jumpShapesTo) {
            if(deviationHashMap.containsKey(shape.getTo())){
                deviationHashMap.put(shape.getTo(),deviationHashMap.get(shape.getTo())+30.0);
            }else{
                deviationHashMap.put(shape.getTo(),30.0);
            }
            shape.setDeviation(deviationHashMap.get(shape.getTo()));
            shape.updatePos();
        }
    }

    public void addJumpFrom(Jump jump){
        jumpShapesFrom.add(jump);
    }

    public void removeJump(Jump jump){
        jumpShapesTo.remove(jump);
        jumpShapesFrom.remove(jump);
    }

    public void updatePos(double x,double y){
        setCenterX(x);
        setCenterY(y);
        for (Jump jump : jumpShapesTo) {
            jump.updatePos();
        }
        for (Jump jump : jumpShapesFrom) {
            jump.updatePos();
        }
    }

    public ArrayList<Jump> getJumpShapesTo() {
        return jumpShapesTo;
    }

    private int proto_id=0;
    public ProtoQuest.Location.Builder compileProto(int id){
        proto_id=id;
        ProtoQuest.Location.Builder builder = ProtoQuest.Location.newBuilder();
        builder.setText(locationText);
        builder.setCode(locationCode);
        builder.setX(getCenterX());
        builder.setY(getCenterY());
        builder.setId(id);
        builder.setType(type.name());
        return builder;
    }

    public int getProto_id() {
        return proto_id;
    }


    public LocationType getType() {
        return type;
    }

    public void setType(LocationType type) {
        this.type = type;
        updateColor();
    }

    public void updateColor(){
        Color color = Color.BLACK;
        if(type==LocationType.NORMAL){
            color= Color.BLACK;
        }
        if(type==LocationType.START){
            color=Color.BLUE;
        }
        if(type==LocationType.VICTORY){
            color=Color.LIGHTGREEN;
        }
        if(type==LocationType.DEFEAT){
            color=Color.RED;
        }
        if(locationText.length()==0){
            color=color.deriveColor(1,1,1,0.5);
        }

        setFill(color);
    }
}