package game.space.quest.editor;

import game.space.quest.editor.proto.ProtoQuest;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.QuadCurve;
import javafx.scene.text.TextAlignment;


public class Jump extends QuadCurve {
    private static final JumpEdit jumpEdit=new JumpEdit();
    private double deviation;
    private Location from;
    private Location to;
    private QuestMap parent;
    private Tooltip tooltip;

    private static Jump clip_copy=null;

    public Jump(QuestMap parent, Location from, Location to) {
        this.parent = parent;
        this.from = from;
        this.to = to;
        updatePos();

        tooltip = new Tooltip();
        tooltip.setTextAlignment(TextAlignment.CENTER);
        tooltip.setWrapText(true);
        tooltip.setMaxWidth(800);

        updateColor();
        setStrokeWidth(3);
        setFill(null);
        setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                bold(mouseEvent.getScreenX(),mouseEvent.getScreenY());

            }
        });

        setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                unbold();
            }
        });

        setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2) {
                        edit();
                    }
                }
            }
        });

        ContextMenu contextMenu = new ContextMenu();
        MenuItem menuItem = new MenuItem("Delete");
        MenuItem menuItem1 = new MenuItem("Edit");
        MenuItem menuItem2 = new MenuItem("Copy");
        MenuItem menuItem3 = new MenuItem("Paste");
        contextMenu.getItems().addAll(menuItem1,menuItem2,menuItem3,menuItem);

        menuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                delete();
            }
        });
        menuItem1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                edit();
            }
        });
        menuItem2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                clip_copy=Jump.this;
            }
        });
        menuItem3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Jump.this.setJumpText(clip_copy.getJumpText());
                Jump.this.setJumpCode(clip_copy.getJumpCode());
                Jump.this.setPriorityCode(clip_copy.getPriorityCode());
                Jump.this.setJumpCondition(clip_copy.getJumpCondition());
            }
        });


        setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(clip_copy==null){
                    menuItem3.setDisable(true);
                }else{
                    menuItem3.setDisable(false);
                }
                if(mouseEvent.isSecondaryButtonDown()){
                    contextMenu.show(Jump.this,mouseEvent.getScreenX(),mouseEvent.getScreenY());
                }
            }
        });

        updateTooltipText();
    }

    public void bold(){
        setStrokeWidth(5);
        Bounds bounds = localToScene(getBoundsInLocal());
        tooltip.show(parent,
                ((bounds.getMinX()+bounds.getMaxX())/2)-tooltip.getWidth()/2,
                ((bounds.getMinY()+bounds.getMaxY())/2)-tooltip.getHeight()*1.1);
        tooltip.show(parent,
                ((bounds.getMinX()+bounds.getMaxX())/2)-tooltip.getWidth()/2,
                ((bounds.getMinY()+bounds.getMaxY())/2)-tooltip.getHeight()*1.1);
    }

    public void bold(double mousex,double mousey){
        setStrokeWidth(5);
        tooltip.show(parent,
                mousex-tooltip.getWidth()/2,
                mousey-tooltip.getHeight()*1.1);
    }


    public void unbold(){
        setStrokeWidth(3);
        tooltip.hide();
    }

    public void delete(){
        parent.deleteJump(this);
        from.updateJumpDeviations();
    }

    public void edit(){
        jumpEdit.load(this);
    }

    private String jumpText="",
            priorityCode="return 0",
            jumpCondition="return true",
            jumpCode="";

    public String getPriorityCode() {
        return priorityCode;
    }

    public void setPriorityCode(String priorityCode) {
        this.priorityCode = priorityCode;
    }

    public void setDeviation(double deviation) {
        this.deviation = deviation;
        updatePos();
    }

    public String getJumpText() {
        return jumpText;
    }

    public void setJumpText(String jumpText) {
        this.jumpText = jumpText;
        updateTooltipText();
        updateColor();
    }

    public String getJumpCondition() {
        return jumpCondition;
    }

    public void setJumpCondition(String jumpCondition) {
        this.jumpCondition = jumpCondition;
        updateTooltipText();
    }

    public String getJumpCode() {
        return jumpCode;
    }

    public void setJumpCode(String jumpCode) {
        this.jumpCode = jumpCode;
        updateTooltipText();
    }


    private void updateTooltipText(){
        String jt,jprior,jcond,jcode;
        if(jumpText.length()>400){
            jt=jumpText.substring(0,400)+"...";
        }else{
            jt=jumpText;
        }
        if(priorityCode.length()>400){
            jprior=priorityCode.substring(0,400)+"...";
        }else{
            jprior=priorityCode;
        }
        if(jumpCondition.length()>400){
            jcond=jumpCondition.substring(0,400)+"...";
        }else{
            jcond=jumpCondition;
        }
        if(jumpCode.length()>400){
            jcode=jumpCode.substring(0,400)+"...";
        }else{
            jcode=jumpCode;
        }
        tooltip.setText(jt+"\n·\n"+jprior+"\n·\n"+jcond+"\n·\n"+jcode);
    }


    public void updatePos(){
        setStartX(from.getCenterX());
        setStartY(from.getCenterY());
        double centerx=(from.getCenterX()+ to.getCenterX())/2;
        double centery=(from.getCenterY()+ to.getCenterY())/2;
        double angle=Math.atan2(to.getCenterY()- from.getCenterY(), to.getCenterX()- from.getCenterX())-Math.PI/2;
        setControlX(centerx+Math.cos(angle)*deviation);
        setControlY(centery+Math.sin(angle)*deviation);
        setEndX(to.getCenterX());
        setEndY(to.getCenterY());
    }


    public Location getFrom() {
        return from;
    }

    public Location getTo() {
        return to;
    }


    public ProtoQuest.Jump.Builder compileProto(){
        ProtoQuest.Jump.Builder builder = ProtoQuest.Jump.newBuilder();
        builder.setText(jumpText);
        builder.setPriority(priorityCode);
        builder.setCondition(jumpCondition);
        builder.setCode(jumpCode);
        builder.setFrom(from.getProto_id());
        builder.setTo(to.getProto_id());
        return builder;
    }

    public void updateColor(){
        Color color=Color.gray(0.3);
        if(jumpText.length()==0){
            color=color.deriveColor(1,1,1,0.5);
        }

        setStroke(color);
    }
}