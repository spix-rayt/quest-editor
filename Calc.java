package game.space.quest.editor;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.JsePlatform;

/**
 * Created by spix-rayt on 3/6/16.
 */
public class Calc extends Stage {
    private static double width= Screen.getPrimary().getVisualBounds().getWidth()*0.7;
    private static double height=Screen.getPrimary().getVisualBounds().getHeight()*0.7;
    private final TextArea scriptText;
    private final Label label;

    public Calc() {
        scriptText = new TextArea();
        scriptText.setWrapText(true);
        scriptText.setStyle("-fx-font-family: monospace");
        BorderPane root=new BorderPane();

        Button closeButton = new Button("Calc");
        label = new Label();
        closeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                calculate(true);
            }
        });


        scriptText.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if(keyEvent.getCode()==KeyCode.ESCAPE){
                    Calc.this.close();
                }
            }
        });
        root.setCenter(scriptText);
        BorderPane borderPane = new BorderPane();
        borderPane.setRight(closeButton);
        borderPane.setLeft(label);
        root.setBottom(borderPane);

        Scene scene=new Scene(root,width,height);
        setScene(scene);

        scriptText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                calculate(false);
            }
        });

        setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                width=scene.getWidth();
                height=scene.getHeight();
            }
        });
        show();
    }


    public void calculate(boolean alert){
        try {
            Globals globals = JsePlatform.standardGlobals();
            LuaValue call = globals.load(scriptText.getText()).call();
            label.setText(call.toString());
        }catch (LuaError e){
            label.setText("error");
            if(alert){
                alertError(e);
            }
        }
    }

    private void alertError(LuaError e) {
        Alert alert=new Alert(Alert.AlertType.ERROR);
        alert.initStyle(StageStyle.UNDECORATED);
        alert.setTitle("Lua error");
        alert.setContentText(e.getMessage());
        alert.setResizable(true);
        alert.getDialogPane().setPrefSize(480, 320);
        alert.showAndWait();
    }
}
