package game.space.quest.editor;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


public class JumpEdit extends Stage {
    private static double width= Screen.getPrimary().getVisualBounds().getWidth()*0.7;
    private static double height=Screen.getPrimary().getVisualBounds().getHeight()*0.7;
    private final TextArea jumpText;
    private final TextArea jumpCode;
    private final Button saveButton;
    private final BorderPane root;
    private final TextArea jumpPriority;
    private final TextArea jumpCondition;

    public JumpEdit() {
        SplitPane splitPane=new SplitPane();
        splitPane.setOrientation(Orientation.VERTICAL);


        jumpText = new TextArea();
        jumpText.setWrapText(true);
        jumpPriority = new TextArea("");
        jumpPriority.setWrapText(true);
        jumpPriority.setStyle("-fx-font-family: monospace");
        jumpCondition = new TextArea("");
        jumpCondition.setWrapText(true);
        jumpCondition.setStyle("-fx-font-family: monospace");
        jumpCode = new TextArea();
        jumpCode.setWrapText(true);
        jumpCode.setStyle("-fx-font-family: monospace");
        BorderPane borderPane=new BorderPane();
        HBox hBox=new HBox();
        saveButton = new Button("Save");


        Button closeButton = new Button("Close");
        closeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                JumpEdit.this.close();
            }
        });
        hBox.getChildren().addAll(saveButton,closeButton);
        borderPane.setRight(hBox);
        splitPane.getItems().addAll(jumpText,jumpPriority,jumpCondition, jumpCode);
        splitPane.setDividerPosition(0,0);
        splitPane.setDividerPosition(1,0);


        root = new BorderPane();
        root.setCenter(splitPane);
        root.setBottom(borderPane);
        Scene scene=new Scene(root,width,height);
        setScene(scene);
    }

    private EventHandler<KeyEvent> keyEventHandler(final Jump jump, final TextArea jumpText,final TextArea jumpPriority,final TextArea jumpCondition, final TextArea jumpCode) {
        return new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if(keyEvent.getCode()== KeyCode.S && keyEvent.isControlDown()){
                    save(jump,jumpText.getText(),jumpPriority.getText(),jumpCondition.getText(),jumpCode.getText());
                }
                if(keyEvent.getCode()==KeyCode.D && keyEvent.isControlDown()){
                    if(keyEvent.getTarget() instanceof TextArea) {
                        TextArea target = (TextArea) keyEvent.getTarget();
                        int indx1 = target.getText().substring(0, target.getCaretPosition()).lastIndexOf("\n")+1;
                        int indx2 = target.getText().indexOf("\n", target.getCaretPosition());
                        if (indx1 < 0) {
                            indx1 = 0;
                        }
                        if (indx2 < 0) {
                            indx2 = target.getText().length();
                        }
                        String for_dupl = target.getText().substring(indx1, indx2);
                        target.setText(target.getText().substring(0, indx1) + for_dupl + "=" + for_dupl + target.getText().substring(indx2));
                        target.positionCaret((target.getText().substring(0, indx1) + for_dupl + "=" + for_dupl).length());
                    }
                }
                if(keyEvent.getCode()==KeyCode.ESCAPE){
                    JumpEdit.this.close();
                }
            }
        };
    }

    private EventHandler<ActionEvent> actionEventHandler(final Jump jump, final TextArea jumpText,final TextArea jumpPriority,final TextArea jumpCondition, final TextArea jumpCode) {
        return new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                save(jump,jumpText.getText(),jumpPriority.getText(),jumpCondition.getText(),jumpCode.getText());
            }
        };
    }

    private void save(Jump jump,String jumpText,String jumpPriority,String jumpCondition,String jumpCode){
        jump.setJumpText(jumpText);
        jump.setPriorityCode(jumpPriority);
        jump.setJumpCondition(jumpCondition);
        jump.setJumpCode(jumpCode);
        JumpEdit.this.hide();
    }

    public void load(Jump jump){
        jumpText.setText(jump.getJumpText());
        jumpPriority.setText(jump.getPriorityCode());
        jumpCode.setText(jump.getJumpCode());
        jumpCondition.setText(jump.getJumpCondition());

        saveButton.setOnAction(actionEventHandler(jump, jumpText,jumpPriority,jumpCondition, jumpCode));
        jumpText.setOnKeyPressed(keyEventHandler(jump, jumpText,jumpPriority,jumpCondition, jumpCode));
        jumpPriority.setOnKeyPressed(keyEventHandler(jump, jumpText,jumpPriority,jumpCondition, jumpCode));
        jumpCondition.setOnKeyPressed(keyEventHandler(jump, jumpText,jumpPriority,jumpCondition, jumpCode));
        jumpCode.setOnKeyPressed(keyEventHandler(jump, jumpText,jumpPriority,jumpCondition, jumpCode));
        show();
    }
}
