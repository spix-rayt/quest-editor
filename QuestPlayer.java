package game.space.quest.editor;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.StageStyle;
import org.luaj.vm2.*;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.jse.JsePlatform;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Random;



public class QuestPlayer extends BorderPane {

    private final Label questText;
    private final VBox vBoxButtons;
    private Globals lua;
    private final Label statsText;
    private final Circle circle;
    private boolean debug=true;
    private boolean fullplayer=false;

    public boolean isFullplayer() {
        return fullplayer;
    }

    public void setFullplayer(boolean fullplayer) {
        this.fullplayer = fullplayer;
    }

    public Circle getCircle() {
        return circle;
    }

    public QuestPlayer() {
        circle=new Circle();
        circle.setVisible(false);
        circle.setRadius(12);
        circle.setFill(null);
        circle.setStroke(Color.BLUEVIOLET);
        circle.setStrokeWidth(2);
        SplitPane splitPane1 = new SplitPane();
        splitPane1.setOrientation(Orientation.VERTICAL);
        ScrollPane questTextScrollPane = new ScrollPane();
        questText = new Label();
        questTextScrollPane.setContent(questText);
        questTextScrollPane.setFitToWidth(true);
        questText.setWrapText(true);


        SplitPane splitPane2 = new SplitPane();
        ScrollPane questButtonsScrollPane = new ScrollPane();
        vBoxButtons = new VBox();
        vBoxButtons.prefWidthProperty().bind(questButtonsScrollPane.widthProperty());
        questButtonsScrollPane.setContent(vBoxButtons);
        ScrollPane questStatsScrollPane = new ScrollPane();
        statsText = new Label();
        statsText.setWrapText(true);
        questStatsScrollPane.setContent(statsText);
        ContextMenu contextMenu = new ContextMenu();
        MenuItem menuItem = new MenuItem("Edit");
        MenuItem menu_debug = new MenuItem();
        menuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                new ScriptEdit(questMap.getStatsInfoScript());
            }
        });
        menu_debug.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                debug=!debug;
                updateStats();
            }
        });
        contextMenu.getItems().addAll(menuItem,menu_debug);
        questStatsScrollPane.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.isSecondaryButtonDown()) {
                    contextMenu.show(questStatsScrollPane, mouseEvent.getScreenX(), mouseEvent.getScreenY());
                }else{
                    contextMenu.hide();
                }
                menuItem.setDisable(fullplayer);
                if(debug){
                    menu_debug.setText("Debug off");
                }else{
                    menu_debug.setText("Debug on");
                }
            }
        });
        splitPane1.getItems().addAll(questTextScrollPane,splitPane2);
        splitPane1.setDividerPosition(0,0.75);
        splitPane2.getItems().addAll(questButtonsScrollPane,questStatsScrollPane);
        splitPane2.setDividerPosition(0,0.75);
        setCenter(splitPane1);
    }

    public QuestMap questMap;
    private Random random;
    private LinkedList<State> states=new LinkedList<>();
    public void play(){
        random=new Random();
        for(int i=0;i<100;i++){
            random.nextDouble();
        }

        try {
            lua = JsePlatform.standardGlobals();
            states.clear();


            lua.set("info", new OneArgFunction() {
                @Override
                public LuaValue call(LuaValue luaValue) {
                    String concat = statsText.getText().concat(luaValue.toString() + "\n");
                    statsText.setText(concat);
                    return LuaValue.valueOf(concat);
                }
            });

            lua.set("defeat", new OneArgFunction() {
                @Override
                public LuaValue call(LuaValue luaValue) {
                    Location location = new Location(questMap, currentLocation.getCenterX(), currentLocation.getCenterY());
                    location.setLocationText(luaValue.checkstring().toString());
                    location.setType(Location.LocationType.DEFEAT);
                    Jump jump = new Jump(questMap, currentLocation, location);
                    jump.setJumpText("...");
                    edge_defeat=jump;
                    return LuaValue.valueOf(true);
                }
            });
        }catch (LuaError e){
            alertError(e);
        }


        Location startLocation = questMap.findStartLocation();
        if (startLocation == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initStyle(StageStyle.UNDECORATED);
            alert.setContentText("Need start location");
            alert.showAndWait();
        } else {
            lua.load(questMap.getStartScript().getScript()).call();
            edgeCheck();
            playLocation(startLocation);
        }
    }

    public Globals copyGlobals(Globals globals){
        Globals new_globals = JsePlatform.standardGlobals();
        for (LuaValue luaKey : globals.keys()) {
            LuaValue luaValue = globals.get(luaKey);
            if(luaValue.istable()){
                if(luaKey.neq_b(LuaValue.valueOf("package")) && luaKey.neq_b(LuaValue.valueOf("_G"))) {
                    new_globals.set(luaKey, copyTable(luaValue.checktable()));
                }
            }else {
                new_globals.set(luaKey, luaValue);
            }
        }
        return new_globals;
    }

    public LuaTable copyTable(LuaTable luaTable){
        LuaTable new_luaTable = new LuaTable();
        for (LuaValue luaKey : luaTable.keys()) {
            LuaValue luaValue = luaTable.get(luaKey);
            if(luaValue.istable()){
                new_luaTable.set(luaKey, copyTable(luaValue.checktable()));
            }else {
                new_luaTable.set(luaKey, luaValue);
            }
        }
        return new_luaTable;
    }

    public void updateStats(){
        try {
            statsText.setText("");
            lua.load(questMap.getStatsInfoScript().getScript()).call();

            if (debug && !fullplayer) {
                StringBuilder stringBuilder = new StringBuilder(statsText.getText() + "\n\n-------------\n");

                for (LuaValue luaName : lua.keys()) {
                    LuaValue luaValue = lua.get(luaName);
                    if (luaValue.isnumber()) {
                        stringBuilder.append(luaName.toString() + "=" + luaValue.tonumber() + "\n");
                    }
                    if (luaValue.isboolean()) {
                        stringBuilder.append(luaName.toString() + "=" + luaValue.toboolean() + "\n");
                    }
                }
                statsText.setText(stringBuilder.toString());
            }
        }catch (LuaError e){
            alertError(e);
        }
    }

    public void stateUndo(){
        if(states.size()>0) {
            State state = states.pollLast();
            lua=state.globals;
            currentLocation=state.location;
            updateLocation();
        }
    }


    private Location currentLocation=null;
    public void playLocation(Location location){
        try {
            currentLocation=location;
            lua.load(location.getLocationCode()).call();
            if(location.getType() == Location.LocationType.NORMAL || location.getType() == Location.LocationType.START) {
                edgeCheck();
            }
            updateLocation();
        }catch (StackOverflowError stackOverflowError){
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    alertInfinityCycle();
                }
            });
            stackOverflowError.printStackTrace();
        }catch (LuaError e){
            location.edit();
            alertError(e);
        }
    }

    private Jump edge_defeat=null;
    public void edgeCheck(){
        try {
            lua.load(questMap.getEdgeScript().getScript()).call();
        }catch (LuaError e){
            alertError(e);
        }
    }

    public void updateLocation(){
        if(currentLocation!=null) {
            circle.setCenterX(currentLocation.getCenterX());
            circle.setCenterY(currentLocation.getCenterY());
            circle.setVisible(true);
            questMap.centerScroll(circle);
            updateStats();
            String locationText = currentLocation.getLocationText();

            String[] split = locationText.split("[\\{\\}]");
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < split.length; i++) {
                if (i % 2 == 0) {
                    stringBuilder.append(split[i]);
                }
                if (i % 2 == 1) {
                    stringBuilder.append(lua.load("return " + split[i]).call().toString());
                }
            }
            if(stringBuilder.length()>0) {
                questText.setText(stringBuilder.toString());
            }

            if(currentLocation.getType()== Location.LocationType.DEFEAT){
                questText.setText(questText.getText()+"\n\n DEFEAT");
            }

            if(currentLocation.getType()== Location.LocationType.VICTORY){
                questText.setText(questText.getText()+"\n\n VICTORY");
            }

            clearJumps();
            if(currentLocation.getType()!= Location.LocationType.DEFEAT && currentLocation.getType()!= Location.LocationType.VICTORY) {
                if (edge_defeat == null) {
                    ArrayList<Label> labels = new ArrayList<>();
                    int count_empty_jumps = 0;
                    for (Jump jump : currentLocation.getJumpShapesTo()) {
                        LuaValue condition = lua.load(jump.getJumpCondition()).call();
                        if (condition != null && condition.eq_b(LuaValue.valueOf(true))) {
                            labels.add(getJumpButton(jump));
                            if (jump.getJumpText().length() == 0) {
                                count_empty_jumps++;
                            }
                        }
                    }

                    if (count_empty_jumps == labels.size() && labels.size() > 0) {
                        int i = random.nextInt(labels.size());
                        Label label = labels.get(i);
                        Jump userData = (Jump) label.getUserData();
                        playJump(userData, false);
                    } else {
                        labels.sort(new Comparator<Label>() {
                            @Override
                            public int compare(Label l1, Label l2) {
                                Jump j1 = (Jump) l1.getUserData();
                                Jump j2 = (Jump) l2.getUserData();
                                double n1 = lua.load(j1.getPriorityCode()).call().todouble();
                                double n2 = lua.load(j2.getPriorityCode()).call().todouble();
                                return Double.compare(n1, n2);
                            }
                        });
                        vBoxButtons.getChildren().addAll(labels);
                    }
                } else {
                    vBoxButtons.getChildren().addAll(getJumpButton(edge_defeat));
                    edge_defeat = null;
                }
            }
        }
    }

    private void alertError(LuaError e) {
        Alert alert=new Alert(Alert.AlertType.ERROR);
        alert.initStyle(StageStyle.UNDECORATED);
        alert.setTitle("Lua error");
        alert.setContentText(e.getMessage());
        alert.setResizable(true);
        alert.getDialogPane().setPrefSize(480, 320);
        alert.showAndWait();
    }

    private void alertInfinityCycle(){
        Alert alert=new Alert(Alert.AlertType.ERROR);
        alert.initStyle(StageStyle.UNDECORATED);
        alert.setTitle("Infinity cycle");
        alert.setContentText("Infinity cycle");
        alert.setResizable(true);
        alert.getDialogPane().setPrefSize(480, 320);
        alert.showAndWait();
    }

    public void clearJumps(){
        vBoxButtons.getChildren().clear();
    }

    public Label getJumpButton(Jump jump){
        Label label=new Label(jump.getJumpText());
        label.setWrapText(true);
        label.setStyle("-fx-label-padding:5;-fx-background-color:#cccccc;-fx-border-insets: 1px;-fx-background-insets: 1px;");
        label.prefWidthProperty().bind(vBoxButtons.prefWidthProperty());
        label.setUserData(jump);

        label.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                label.setStyle("-fx-label-padding:5;-fx-background-color:#888888;-fx-border-insets: 1px;-fx-background-insets: 1px;");
                if(!fullplayer) {
                    jump.bold();
                }
            }
        });
        label.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                label.setStyle("-fx-label-padding:5;-fx-background-color:#cccccc;-fx-border-insets: 1px;-fx-background-insets: 1px;");
                jump.unbold();
            }
        });
        label.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                playJump(jump,true);
            }
        });
        return label;
    }

    public void playJump(Jump jump,boolean saveState){
        try {
            if(saveState) {
                states.add(new State(lua, jump.getFrom()));
            }
            lua.load(jump.getJumpCode()).call();
            edgeCheck();
            if(edge_defeat==null) {
                playLocation(jump.getTo());
            }else{
                playLocation(edge_defeat.getTo());
                edge_defeat=null;
            }
        }catch (LuaError e){
            jump.edit();
            alertError(e);
        }
    }

    public class State{
        public Globals globals;
        public Location location;

        public State(Globals globals, Location location) {
            this.globals = copyGlobals(globals);
            this.location = location;
        }
    }
}
