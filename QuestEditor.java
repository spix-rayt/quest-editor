package game.space.quest.editor;

import game.space.quest.editor.proto.ProtoQuest;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Optional;
import java.util.Properties;


public class QuestEditor extends Application {

    private QuestMap questMap;
    private Thread thread;
    private File questFile=null;

    @Override
    public void start(Stage stage) throws Exception {
        BorderPane root = new BorderPane();


        SplitPane splitPane=new SplitPane();
        questMap = new QuestMap();
        QuestPlayer questPlayer=new QuestPlayer();
        questPlayer.questMap=questMap;
        splitPane.getItems().addAll(questPlayer, questMap);
        root.setCenter(splitPane);
        splitPane.setDisable(true);
        splitPane.setVisible(false);


        HBox hBox=new HBox();
        Button newDocument = new Button("",new ImageView(new Image(getClass().getResourceAsStream("/icons/New Document.png"))));
        Button play = new Button("",new ImageView(new Image(getClass().getResourceAsStream("/icons/Play.png"))));
        Button undo_play = new Button("",new ImageView(new Image(getClass().getResourceAsStream("/icons/go-back.png"))));
        Button calc = new Button("",new ImageView(new Image(getClass().getResourceAsStream("/icons/calc.png"))));
        Button update_play = new Button("",new ImageView(new Image(getClass().getResourceAsStream("/icons/Update.png"))));
        Button start_script = new Button("",new ImageView(new Image(getClass().getResourceAsStream("/icons/Gear Alt.png"))));
        Button edge_script = new Button("",new ImageView(new Image(getClass().getResourceAsStream("/icons/Gear Alt.png"))));
        Button openDocument = new Button("",new ImageView(new Image(getClass().getResourceAsStream("/icons/Import Document.png"))));
        Button saveDocument = new Button("",new ImageView(new Image(getClass().getResourceAsStream("/icons/Save.png"))));
        Button fullplayer = new Button("",new ImageView(new Image(getClass().getResourceAsStream("/icons/Fullscreen.png"))));
        Separator separator = new Separator();
        separator.setPrefWidth(300);
        Separator separator1 = new Separator();
        separator1.setPrefWidth(300);
        hBox.getChildren().addAll(newDocument,openDocument,saveDocument, separator,play,undo_play,update_play,start_script,edge_script,separator1,fullplayer,calc);
        newDocument.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                FileChooser fileChooser=new FileChooser();
                fileChooser.setTitle("New quest file");
                fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Quest File (*.kek)","*.kek"));
                fileChooser.setInitialDirectory(new File("."));
                File file = fileChooser.showSaveDialog(stage);
                if(file!=null){
                    if(!file.getAbsolutePath().endsWith(".kek")){
                        file=new File(file+".kek");
                    }
                    questMap.clear(questPlayer.getCircle());
                    if(trySave(file)){
                        questFile=file;
                        splitPane.setDisable(false);
                        splitPane.setVisible(true);
                    }
                }
            }
        });
        fullplayer.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                boolean fullplayer = !questPlayer.isFullplayer();

                if(fullplayer==true){
                    splitPane.getItems().remove(questMap);
                }else{
                    splitPane.getItems().addAll(questMap);
                }
                undo_play.setDisable(fullplayer);
                calc.setDisable(fullplayer);
                update_play.setDisable(fullplayer);
                start_script.setDisable(fullplayer);
                edge_script.setDisable(fullplayer);


                questPlayer.setFullplayer(fullplayer);
            }
        });

        calc.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                new Calc();
            }
        });
        play.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                questPlayer.play();
            }
        });
        undo_play.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                questPlayer.stateUndo();
            }
        });
        update_play.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                questPlayer.updateLocation();
            }
        });
        start_script.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                new ScriptEdit(questMap.getStartScript());
            }
        });
        edge_script.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                new ScriptEdit(questMap.getEdgeScript());
            }
        });
        openDocument.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                FileChooser fileChooser=new FileChooser();
                fileChooser.setTitle("Open quest file");
                fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Quest File (*.kek)","*.kek"));
                fileChooser.setInitialDirectory(new File("."));
                File file = fileChooser.showOpenDialog(stage);
                if(file!=null){
                    questMap.clear(questPlayer.getCircle());
                    if(tryLoad(file)){
                        questFile=file;
                        splitPane.setDisable(false);
                        splitPane.setVisible(true);
                    }
                }
            }
        });
        saveDocument.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if(questFile!=null) {
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Save quest file");
                    fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Quest File (*.kek)","*.kek"));
                    fileChooser.setInitialDirectory(new File("."));
                    if(questFile!=null){
                        fileChooser.setInitialFileName(questFile.getName());
                    }
                    File file = fileChooser.showSaveDialog(stage);
                    if (file != null) {
                        if(!file.getAbsolutePath().endsWith(".kek")){
                            file=new File(file+".kek");
                        }
                        if (trySave(file)) {
                            questFile = file;
                        }
                    }
                }
            }
        });
        root.setTop(hBox);


        Scene scene=new Scene(root,800,600);
        stage.setScene(scene);
        stage.setMaximized(true);
        Platform.setImplicitExit(false);
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                if(questFile==null){
                    thread.interrupt();
                    Platform.exit();
                    return;
                }
                if(questFile.getName().equals("autoopen.kek")){
                    thread.interrupt();
                    saveHotKeyEvent(false);
                    Platform.exit();
                    return;
                }
                if(questFile!=null) {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Save file?", ButtonType.NO, ButtonType.CANCEL, ButtonType.YES);
                    Optional<ButtonType> buttonType = alert.showAndWait();
                    if (buttonType.get() == ButtonType.YES) {
                        thread.interrupt();
                        saveHotKeyEvent(false);
                        Platform.exit();
                    }
                    if (buttonType.get() == ButtonType.NO) {
                        thread.interrupt();
                        Platform.exit();
                    }
                    if(buttonType.get()==ButtonType.CANCEL){
                        windowEvent.consume();
                    }
                }
            }
        });

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(120000);
                    } catch (InterruptedException e) {
                        return;
                    }
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (!new File("backup/").exists()) {
                                    new File("backup/").mkdirs();
                                }
                                trySave(new File("backup/" + System.currentTimeMillis() + ".kek"));
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if(keyEvent.getCode()== KeyCode.S && keyEvent.isControlDown()){
                    saveHotKeyEvent(true);
                }
            }
        });

        File file = new File("autoopen.kek");
        if(file.exists()){
            questMap.clear(questPlayer.getCircle());
            if(tryLoad(file)){
                questFile=file;
                splitPane.setDisable(false);
                splitPane.setVisible(true);
            }
        }
        thread.start();
        stage.show();
    }

    public void saveHotKeyEvent(boolean showalert){
        if(trySave(questFile) && showalert) {
            new Alert(Alert.AlertType.INFORMATION, "File saved", ButtonType.OK).showAndWait();
        }
    }

    public boolean trySave(File file){
        try {
            ProtoQuest.Quest.Builder quest_builder = ProtoQuest.Quest.newBuilder();
            int loc_id=0;
            for (Node node : questMap.getPane().getChildren()) {
                if(node instanceof Location){
                    Location location = (Location) node;
                    quest_builder.addLocations(location.compileProto(loc_id));
                    loc_id++;
                }
            }

            for (Node node : questMap.getPane().getChildren()) {
                if(node instanceof Jump){
                    Jump jump= ((Jump) node);
                    quest_builder.addJumps(jump.compileProto());
                }
            }

            quest_builder.setStartScript(questMap.getStartScript().getScript());
            quest_builder.setStatsInfoScript(questMap.getStatsInfoScript().getScript());
            quest_builder.setEdgeScript(questMap.getEdgeScript().getScript());


            quest_builder.build().writeTo(new FileOutputStream(file));
        }catch (Exception e){
            new Alert(Alert.AlertType.ERROR,"Saving error").showAndWait();
            e.printStackTrace();
            return false;
        }
        return  true;
    }

    public boolean tryLoad(File file){
        ProtoQuest.Quest quest=null;
        try {
            quest = ProtoQuest.Quest.parseFrom(new FileInputStream(file));
            HashMap<Integer, Location> locationHashMap = new HashMap<>();
            for (Iterator<ProtoQuest.Location> iterator = quest.getLocationsList().iterator(); iterator.hasNext(); ) {
                ProtoQuest.Location next = iterator.next();
                Location location = questMap.createLocation(next.getX(), next.getY());
                location.setLocationText(next.getText());
                location.setLocationCode(next.getCode());
                location.setType(Location.LocationType.valueOf(next.getType()));
                locationHashMap.put(next.getId(), location);
            }

            for (Iterator<ProtoQuest.Jump> iterator = quest.getJumpsList().iterator(); iterator.hasNext(); ) {
                ProtoQuest.Jump next = iterator.next();
                Jump jump = questMap.createJump(locationHashMap.get(next.getFrom()), locationHashMap.get(next.getTo()));
                jump.setJumpText(next.getText());
                jump.setPriorityCode(next.getPriority());
                jump.setJumpCondition(next.getCondition());
                jump.setJumpCode(next.getCode());
            }
            if (quest.hasStartScript()) {
                questMap.getStartScript().setScript(quest.getStartScript());
            }
            if (quest.hasStatsInfoScript()) {
                questMap.getStatsInfoScript().setScript(quest.getStatsInfoScript());
            }
            if(quest.hasEdgeScript()){
                questMap.getEdgeScript().setScript(quest.getEdgeScript());
            }
        }catch (Exception e){
            new Alert(Alert.AlertType.ERROR,"Loading error").showAndWait();
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        launch();
    }
}
