package game.space.quest.editor;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Created by spix-rayt on 3/6/16.
 */
public class ScriptEdit extends Stage {
    private static double width= Screen.getPrimary().getVisualBounds().getWidth()*0.7;
    private static double height=Screen.getPrimary().getVisualBounds().getHeight()*0.7;
    private final TextArea scriptText;
    private final Button saveButton;

    public ScriptEdit(Script script) {
        scriptText = new TextArea(script.getScript());
        scriptText.setWrapText(true);
        scriptText.setStyle("-fx-font-family: monospace");
        BorderPane root=new BorderPane();

        HBox hBox=new HBox();
        saveButton = new Button("Save");
        Button closeButton = new Button("Close");
        saveButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                script.setScript(scriptText.getText());
                ScriptEdit.this.close();
            }
        });
        closeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                ScriptEdit.this.close();
            }
        });

        scriptText.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if(keyEvent.getCode()== KeyCode.S && keyEvent.isControlDown()){
                    script.setScript(scriptText.getText());
                    ScriptEdit.this.close();
                }
                if(keyEvent.getCode()==KeyCode.ESCAPE){
                    ScriptEdit.this.close();
                }
            }
        });
        hBox.getChildren().addAll(saveButton,closeButton);
        root.setCenter(scriptText);
        BorderPane borderPane = new BorderPane();
        borderPane.setRight(hBox);
        root.setBottom(borderPane);

        Scene scene=new Scene(root,width,height);
        setScene(scene);

        setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                width=scene.getWidth();
                height=scene.getHeight();
            }
        });
        show();
    }
}
