package game.space.quest.editor;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;



public class LocationEdit extends Stage {
    private static double width= Screen.getPrimary().getVisualBounds().getWidth()*0.7;
    private static double height=Screen.getPrimary().getVisualBounds().getHeight()*0.7;
    private final TextArea locationText;
    private final TextArea locationCode;
    private final Button saveButton;
    private final BorderPane root;

    public LocationEdit() {
        SplitPane splitPane=new SplitPane();
        splitPane.setOrientation(Orientation.VERTICAL);


        locationText = new TextArea();
        locationText.setWrapText(true);
        locationCode = new TextArea();
        locationCode.setWrapText(true);
        locationCode.setStyle("-fx-font-family: monospace");
        BorderPane borderPane=new BorderPane();
        HBox hBox=new HBox();
        saveButton = new Button("Save");

        Button closeButton = new Button("Close");
        closeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                LocationEdit.this.close();
            }
        });
        hBox.getChildren().addAll(saveButton,closeButton);
        borderPane.setRight(hBox);
        splitPane.getItems().addAll(locationText, locationCode);


        root = new BorderPane();
        root.setCenter(splitPane);
        root.setBottom(borderPane);
        Scene scene=new Scene(root,width,height);
        setScene(scene);

        setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                width=scene.getWidth();
                height=scene.getHeight();
            }
        });
    }

    private EventHandler<KeyEvent> keyEventHandler(final Location location, final TextArea locationText, final TextArea locationCode) {
        return new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if(keyEvent.getCode()== KeyCode.S && keyEvent.isControlDown()){
                    save(location,locationText.getText(),locationCode.getText());
                }
                if(keyEvent.getCode()==KeyCode.D && keyEvent.isControlDown()){
                    if(keyEvent.getTarget() instanceof TextArea) {
                        TextArea target = (TextArea) keyEvent.getTarget();
                        int indx1 = target.getText().substring(0, target.getCaretPosition()).lastIndexOf("\n")+1;
                        int indx2 = target.getText().indexOf("\n", target.getCaretPosition());
                        if (indx1 < 0) {
                            indx1 = 0;
                        }
                        if (indx2 < 0) {
                            indx2 = target.getText().length();
                        }
                        String for_dupl = target.getText().substring(indx1, indx2);
                        target.setText(target.getText().substring(0, indx1) + for_dupl + "=" + for_dupl + target.getText().substring(indx2));
                        target.positionCaret((target.getText().substring(0, indx1) + for_dupl + "=" + for_dupl).length());
                    }
                }
                if(keyEvent.getCode()==KeyCode.ESCAPE){
                    LocationEdit.this.close();
                }
            }
        };
    }

    private EventHandler<ActionEvent> actionEventHandler(final Location location, final TextArea locationText, final TextArea locationCode) {
        return new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                save(location,locationText.getText(),locationCode.getText());
            }
        };
    }

    private void save(Location location,String locationText,String locationCode){
        location.setLocationText(locationText);
        location.setLocationCode(locationCode);
        LocationEdit.this.hide();
    }

    public void load(Location location){
        locationText.setText(location.getLocationText());
        locationCode.setText(location.getLocationCode());
        saveButton.setOnAction(actionEventHandler(location, locationText, locationCode));
        locationText.setOnKeyPressed(keyEventHandler(location, locationText, locationCode));
        locationCode.setOnKeyPressed(keyEventHandler(location, locationText, locationCode));
        show();
    }
}
